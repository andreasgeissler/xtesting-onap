---
stages:
  - lint
  - prepare
  - infrastructure-healthcheck
  - healthcheck
  - smoke-usecases
  - candidate-usecases
  - onap-security
  - deploy
  - downstream

variables:
  GIT_SUBMODULE_STRATEGY: recursive
  ANSIBLE_DOCKER_IMAGE:
    registry.gitlab.com/orange-opensource/lfn/ci_cd/docker_ansible
  ANSIBLE_DOCKER_TAG: latest
  CHAINED_CI_INIT: scripts/chained-ci-tools/chained-ci-init.sh
  use_jumphost: "True"

.syntax_checking: &syntax_docker
  extends: .syntax_checking_tags
  stage: lint
  except:
    - schedules
    - triggers

yaml_linting:
  image: sdesbure/yamllint:latest
  script:
    - "yamllint \
       .gitlab-ci.yml"
  <<: *syntax_docker

ansible_linting:
  image: sdesbure/ansible-lint:latest
  script:
    - "ansible-lint -x ANSIBLE0010 \
        roles"
  <<: *syntax_docker

.runner_tags: &runner_tags
  image: ${ANSIBLE_DOCKER_IMAGE}:${ANSIBLE_DOCKER_TAG}
  extends: .ansible_run_tags

##
# Generic Jobs
##
.get_artifact: &get_artifact
  before_script:
    - chmod 700 .
    - . ./${CHAINED_CI_INIT} -a -i inventory/infra
    - mkdir -p ./results/${run_type}
  after_script:
    - ./scripts/chained-ci-tools/clean.sh

.prepare: &prepare
  script:
    - rm -Rf ./results && mkdir -p ./results
    - ansible-playbook ${ansible_verbose} ${VAULT_OPT}
      -i inventory/infra xtesting-jumphost.yaml
  <<: *get_artifact
  <<: *runner_tags

.run_healthcheck: &run_healthcheck
  script:
    - ansible-playbook ${ansible_verbose} ${VAULT_OPT}
      -i inventory/infra xtesting-healthcheck.yaml
      --extra-vars "run_type=${run_type} run_tiers=${run_tiers} run_timeout=${run_timeout}"
    - grep Result.EX_OK results/${run_tiers}/${run_type}/xtesting.log
  <<: *get_artifact
  timeout: 15 minutes

.manage_artifacts: &manage_artifacts
  artifacts:
    paths:
      - results/${run_tiers}/${run_type}/
    when: always

.run_infrastructure_healthcheck: &run_infrastructure_healthcheck
  script:
    - ansible-playbook ${ansible_verbose} ${VAULT_OPT}
      -i inventory/infra xtesting-healthcheck-k8s.yaml
      --extra-vars "run_type=${run_type} run_tiers=${run_tiers}"
  timeout: 15 minutes

.run_onap_vnf: &run_onap_vnf
  script:
    - ansible-playbook ${ansible_verbose} ${VAULT_OPT}
      -i inventory/infra xtesting-onap-vnf.yaml
      --extra-vars "run_type=${run_type} run_tiers=${run_tiers}"
  <<: *get_artifact
  timeout: 45 minutes

.run_smoke_usecase_robot: &run_smoke_usecase_robot
  script:
    - ansible-playbook ${ansible_verbose} ${VAULT_OPT}
      -i inventory/infra xtesting-healthcheck.yaml
      --extra-vars "run_type=${run_type} run_tiers=${run_tiers} run_timeout=${run_timeout}"
    - grep Result.EX_OK results/${run_tiers}/${run_type}/xtesting.log
  <<: *get_artifact
  timeout: 30 minutes

.run_onap_security: &run_onap_security
  script:
    - ansible-playbook ${ansible_verbose} ${VAULT_OPT}
      -i inventory/infra xtesting-onap-security.yaml
      --extra-vars "run_type=${run_type} run_tiers=${run_tiers}"
  timeout: 15 minutes

.infrastructure_healthcheck: &infrastructure_healthcheck
  variables:
    run_tiers: infrastructure-healthcheck
    run_type: k8s
  stage: infrastructure-healthcheck
  allow_failure: true
  <<: *get_artifact
  <<: *run_infrastructure_healthcheck
  <<: *runner_tags
  <<: *manage_artifacts

.core: &core
  variables:
    run_tiers: healthcheck
    run_type: core
    run_timeout: 240
  allow_failure: true
  stage: healthcheck
  <<: *get_artifact
  <<: *run_healthcheck
  <<: *runner_tags
  <<: *manage_artifacts

.small: &small
  variables:
    run_tiers: healthcheck
    run_type: small
    run_timeout: 240
  stage: healthcheck
  allow_failure: true
  <<: *get_artifact
  <<: *run_healthcheck
  <<: *runner_tags
  <<: *manage_artifacts

.medium: &medium
  variables:
    run_tiers: healthcheck
    run_type: medium
    run_timeout: 240
  stage: healthcheck
  allow_failure: true
  <<: *get_artifact
  <<: *run_healthcheck
  <<: *runner_tags
  <<: *manage_artifacts

.full: &full
  variables:
    run_tiers: healthcheck
    run_type: full
    run_timeout: 240
  stage: healthcheck
  allow_failure: true
  <<: *get_artifact
  <<: *run_healthcheck
  <<: *runner_tags
  <<: *manage_artifacts

.healthdist: &healthdist
  variables:
    run_tiers: healthcheck
    run_type: healthdist
    run_timeout: 600
  stage: healthcheck
  allow_failure: true
  <<: *get_artifact
  <<: *run_healthcheck
  <<: *runner_tags
  <<: *manage_artifacts

.postinstall: &postinstall
  variables:
    run_tiers: healthcheck
    run_type: postinstall
    run_timeout: 600
  stage: healthcheck
  allow_failure: true
  <<: *get_artifact
  <<: *runner_tags
  <<: *run_healthcheck
  <<: *manage_artifacts

.vnf_basic_vm: &vnf_basic_vm
  variables:
    run_tiers: smoke-usecases
    run_type: basic_vm
  stage: smoke-usecases
  <<: *get_artifact
  <<: *run_onap_vnf
  <<: *runner_tags
  <<: *manage_artifacts

.vnf_freeradius_nbi: &vnf_freeradius_nbi
  variables:
    run_tiers: smoke-usecases
    run_type: freeradius_nbi
  stage: smoke-usecases
  <<: *get_artifact
  <<: *run_onap_vnf
  <<: *runner_tags
  <<: *manage_artifacts

.vnf_clearwater_ims: &vnf_clearwater_ims
  variables:
    run_tiers: smoke-usecases
    run_type: clearwater_ims
  stage: smoke-usecases
  <<: *get_artifact
  <<: *run_onap_vnf
  <<: *runner_tags
  <<: *manage_artifacts

.pnf_registrate: &pnf_registrate
  variables:
    run_tiers: smoke-usecases-robot
    run_type: pnf-registrate
    run_timeout: 900
  stage: smoke-usecases
  allow_failure: true
  <<: *get_artifact
  <<: *runner_tags
  <<: *run_smoke_usecase_robot
  <<: *manage_artifacts

.security_root_pods: &security_root_pods
  variables:
    run_tiers: security
    run_type: root_pods
  stage: onap-security
  <<: *get_artifact
  <<: *run_onap_security
  <<: *runner_tags
  <<: *manage_artifacts

.security_unlimitted_pods: &security_unlimitted_pods
  variables:
    run_tiers: security
    run_type: unlimitted_pods
  stage: onap-security
  <<: *get_artifact
  <<: *run_onap_security
  <<: *runner_tags
  <<: *manage_artifacts

.security_cis_kubernetes: &security_cis_kubernetes
  variables:
    run_tiers: security
    run_type: cis_kubernetes
  stage: onap-security
  <<: *get_artifact
  <<: *run_onap_security
  <<: *runner_tags
  <<: *manage_artifacts

.security_http_public_endpoints: &security_http_public_endpoints
  variables:
    run_tiers: security
    run_type: http_public_endpoints
  stage: onap-security
  <<: *get_artifact
  <<: *run_onap_security
  <<: *runner_tags
  <<: *manage_artifacts

.security_jdpw_ports: &security_jdpw_ports
  variables:
    run_tiers: security
    run_type: jdpw_ports
  stage: onap-security
  <<: *get_artifact
  <<: *run_onap_security
  <<: *runner_tags
  <<: *manage_artifacts

.security_kube_hunter: &security_kube_hunter
  variables:
    run_tiers: security
    run_type: kube_hunter
  stage: onap-security
  <<: *get_artifact
  <<: *run_onap_security
  <<: *runner_tags
  <<: *manage_artifacts

.security_nonssl_endpoints: &security_nonssl_endpoints
  variables:
    run_tiers: security
    run_type: nonssl_endpoints
  stage: onap-security
  <<: *get_artifact
  <<: *run_onap_security
  <<: *runner_tags
  <<: *manage_artifacts

# triggered PODs
prepare:
  <<: *prepare
  stage: prepare
  only:
    refs:
      - triggers

infrastructure_healthcheck:
  <<: *infrastructure_healthcheck
  only:
    refs:
      - triggers

core:
  <<: *core
  only:
    refs:
      - triggers

small:
  <<: *small
  only:
    variables:
      - $DEPLOYMENT_TYPE == "small"
      - $DEPLOYMENT_TYPE == "medium"
      - $DEPLOYMENT_TYPE == "full"
    refs:
      - triggers

medium:
  <<: *medium
  only:
    variables:
      - $DEPLOYMENT_TYPE == "medium"
      - $DEPLOYMENT_TYPE == "full"
    refs:
      - triggers

full:
  <<: *full
  only:
    variables:
      - $DEPLOYMENT_TYPE == "full"
    refs:
      - triggers

healthdist:
  <<: *healthdist
  only:
    variables:
      - $DEPLOYMENT_TYPE == "full"
    refs:
      - triggers

postinstall:
  <<: *postinstall
  only:
    variables:
      - $DEPLOYMENT_TYPE == "full"
    refs:
      - triggers

vnf_basic_vm:
  <<: *vnf_basic_vm
  only:
    refs:
      - triggers

vnf_freeradius_nbi:
  <<: *vnf_freeradius_nbi
  only:
    refs:
      - triggers

vnf_clearwater_ims:
  <<: *vnf_clearwater_ims
  only:
    refs:
      - triggers

pnf_registrate:
  <<: *pnf_registrate
  only:
    refs:
      - triggers

candidate_usecases:
  stage: candidate-usecases
  script:
    - echo "Candidate usecases to be run here"
  <<: *runner_tags
  <<: *manage_artifacts
  only:
    refs:
      - triggers
  except:
    variables:
      - $pod =~ /gating/

security_root_pods:
  <<: *security_root_pods
  only:
    refs:
      - triggers
  when: always

security_unlimitted_pods:
  <<: *security_unlimitted_pods
  only:
    refs:
      - triggers
  when: always

security_cis_kubernetes:
  <<: *security_cis_kubernetes
  only:
    refs:
      - triggers
  when: always

security_http_public_endpoints:
  <<: *security_http_public_endpoints
  only:
    refs:
      - triggers
  when: always

security_jdpw_ports:
  <<: *security_jdpw_ports
  only:
    refs:
      - triggers
  when: always

security_kube_hunter:
  <<: *security_kube_hunter
  only:
    refs:
      - triggers
  when: always

security_nonssl_endpoints:
  <<: *security_nonssl_endpoints
  only:
    refs:
      - triggers
  when: always

pages:
  stage: deploy
  <<: *get_artifact
  <<: *runner_tags
  script:
    - if [ -z "$GERRIT_REVIEW" ]; then TARGET_DIR=$pod-$CI_JOB_ID-$(date +'%m-%d-%Y_%H-%m'); else TARGET_DIR=$GERRIT_REVIEW-$GERRIT_PATCHSET;fi
    - mkdir -p public/$TARGET_DIR
    - patch_list=$(echo $(find . -regextype posix-extended -regex '^.*[0-9]{5}-[0-9]*') | sed  -e "s/ /,/g" -e "s/\.\///g")
    - ansible-playbook ${ansible_verbose} ${VAULT_OPT}
      -i inventory/infra ./xtesting-pages.yaml
      --extra-vars "patch_list=$patch_list"
    - rsync -avzh --ignore-errors ./doc/ public
    - mv public/index.html public/$TARGET_DIR
    - rsync -avzh --ignore-errors ./results/ public/$TARGET_DIR
    - scripts/output_summary.sh $TARGET_DIR
  artifacts:
    paths:
      - public/
    expire_in: 1 month
  only:
    refs:
      - triggers
  when: always

trigger-view:
  stage: downstream
  image: appropriate/curl:latest
  extends: .ansible_run_tags
  script:
    - curl -X POST
      -F token=${XTESTING_VIEW_TOKEN}
      -F "ref=master"
      -F "variables[UPSTREAM_PIPELINE]=${CI_PIPELINE_URL}"
      -F "variables[ARTIFACT_JOB]=pages"
      https://gitlab.com/api/v4/projects/11448585/trigger/pipeline
  only:
    refs:
      - triggers
  when: always
