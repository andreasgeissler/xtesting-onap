#!/usr/bin/python
#
# This program and the accompanying materials
# are made available under the terms of the Apache License, Version 2.0
# which accompanies this distribution, and is available at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
""" Module to generate Functest reporting for gitlab pages """

import argparse
import datetime
import logging

import jinja2
import requests


# Logger
logging.basicConfig()
LOGGER = logging.getLogger("Xtesting-ONAP-Status")
LOGGER.setLevel("INFO")
# LOGGER.setLevel("DEBUG")

PROXY = {}
# PROXY = {'http': 'socks5h://127.0.0.1:8080',
#          'https': 'socks5h://127.0.0.1:8080'}

# Initialization
URL_PRIVATE_BASE = "http://onap.api.testresults.opnfv.fr/api/v1/results"
URL_BASE = "http://testresults.opnfv.org/onap/api/v1/results"
REPORTINGDATE = datetime.datetime.now().strftime("%Y-%m-%d %H:%M")

# init just connection_check to get the list of scenarios
# as all the scenarios run connection_check
INFRA_HEALTHCHECK = {'name': 'infrastructure-healthcheck',
                     'tests': {'onap-k8s', 'onap-helm'}}
HEALTHCHECK = {'name': 'healthcheck',
               'tests': {'core', 'small', 'medium', 'full',
                         'healthdist', 'postinstall'}}
SMOKE_USECASES = {'name': 'smoke usecases',
                  'tests': {'basic_vm', 'freeradius_nbi', 'clearwater_ims',
                            'pnf_registrate'}}
SECURITY_USECASES = {'name': 'security',
                     'tests': {'root_pods', 'unlimitted_pods', 'cis_kubernetes',
                               'http_public_endpoints', 'jdpw_ports',
                               'kube_hunter'}}

# list of tests with dedicated reporting page to be referenced
RESULT_URLS = {
    'core': './healthcheck/core/core/report.html',
    'small': './healthcheck/small/small/report.html',
    'medium': './healthcheck/medium/medium/report.html',
    'full': './healthcheck/full/full/report.html',
    'postinstall': './healthcheck/postinstall/postinstall/report.html',
    'healthdist': './healthcheck/healthdist/healthdist/report.html',
    'onap-k8s': './infrastructure-healthcheck/k8s/index.html',
    'onap-helm': './infrastructure-healthcheck/k8s/onap-helm.log',
    'basic_vm': './smoke-usecases/basic_vm/xtesting.debug.log',
    'clearwater_ims': './smoke-usecases/clearwater_ims/xtesting.debug.log',
    'freeradius_nbi': './smoke-usecases/freeradius_nbi/xtesting.debug.log',
    'pnf_registrate': './smoke-usecases-robot/pnf-registrate/pnf-registrate/report.html',
    'root_pods': './security/root_pods/root_pods.log',
    'unlimitted_pods': './security/unlimitted_pods/unlimitted_pods.log',
    'cis_kubernetes': './security/cis_kubernetes/cis_kubernetes.log',
    'http_public_endpoints': './security/http_public_endpoints/http_public_endpoints.log',
    'jdpw_ports': './security/jdpw_ports/jdpw_ports.log',
    'kube_hunter': './security/kube_hunter/kube_hunter.log',
    }
# Retrieve the Functest configuration to detect which tests are relevant
# according to the pod, scenario
PERIOD = 8

LOGGER.info("generate Xtesting reporting page")

PARSER = argparse.ArgumentParser()
PARSER.add_argument('-p', '--pod', help='Pod name')
PARSER.add_argument('-d', '--db', help='Test DB URL')
PARSER.add_argument('-t', '--build_tag', help='Build_tag')
ARGS = PARSER.parse_args()

PODS = ['onap_xtesting_openlab-OPNFV-oom',
        'onap_oom_gating_pod4_1-ONAP-oom',
        'onap_oom_gating_pod4_2-ONAP-oom',
        'onap_oom_gating_pod4_3-ONAP-oom',
        'onap_oom_gating_pod4_4-ONAP-oom',
        'onap_oom_gating_azure_1-OPNFV-oom',
        'onap_oom_gating_azure_2-OPNFV-oom',
        'onap_daily_pod4_master-ONAP-oom',
        'onap_daily_pod4_elalto-ONAP-oom']

if ARGS.pod is not None:
    PODS = [ARGS.pod]

LOGGER.info("List of PODS: %s", PODS)
for pod in PODS:
    LOGGER.info("POD: %s", pod)

    TIERS = [INFRA_HEALTHCHECK, HEALTHCHECK, SMOKE_USECASES, SECURITY_USECASES]
    TRENDS = [INFRA_HEALTHCHECK, HEALTHCHECK, SMOKE_USECASES, SECURITY_USECASES]
    TREND_LINE = ""
    SCORE = 0

    # Trend
    # *****
    # calculation of the TREND
    SCORE_TREND = 0
    if ARGS.db is not None:
        URL_BASE = str([ARGS.db][0])
    LOGGER.info("Database: %s", URL_BASE)

    for tier_trend in TRENDS:
        tier_results = []
        nb_tests = 0
        nb_pass = 0
        nb_fail = 0
        score = 0

        for test in tier_trend['tests']:
            project = 'integration'
            # Security tests affected to security project
            if tier_trend['name'] == 'security':
                project = 'security'
            url = (URL_BASE + "?project_name=" + project + "&case=" + test +
                   "&pod_name=" + pod + "&last=5")
            response = requests.get(url, proxies=PROXY)
            response_json = response.json()
            # Note the 'u' must be used in python 2.7
            # str(response_json).count("criteria': 'uFAIL")
            # it shall be removed if using python3
            nb_fail = nb_fail + str(response_json).count("criteria': 'FAIL")
            nb_pass = nb_pass + str(response_json).count("criteria': 'PASS")
        try:
            score_trend = round(100 * nb_pass / (nb_pass + nb_fail))
        except ZeroDivisionError:
            score_trend = 0
        LOGGER.debug("Score Trend %s: %s", tier_trend, score_trend)
        tier_trend['score'] = score_trend

    # calculation of the overall SCORE for TREND
    NB_TIERS = 0
    for tier_trend in TRENDS:
        NB_TIERS += 1
        SCORE_TREND = SCORE_TREND + tier_trend['score']
    SCORE_TREND = round(SCORE_TREND / NB_TIERS)

    LOGGER.info("Score Trend: %s", str(SCORE_TREND))

    # calculation of the overall SCORE
    for tier in TIERS:
        tier_results = []
        nb_tests = 0
        nb_pass = 0
        score = 0
        for test in tier['tests']:
            # for Gating we consider the build_tag to retrieve the results
            # For daily runs, we do not. A build_tag is created based on
            # gitlab CI id and is different for each CI stage
            param_build_tag = ""
            if "gating" in pod and ARGS.build_tag is not None:
                param_build_tag = "&build_tag=" + str([ARGS.build_tag][0])
            project = 'integration'
            # Security tests affected to security project
            if tier['name'] == 'security':
                project = 'security'

            url = (URL_BASE + "?project_name=" + project + "&case=" + test +
                   "&period=" + str(PERIOD) +
                   "&pod_name=" + pod + "&last=1" + param_build_tag)
            LOGGER.debug("url: %s", url)
            response = requests.get(url, proxies=PROXY)
            response_json = response.json()
            response_url = ""
            if test in RESULT_URLS:
                response_url = RESULT_URLS[test]

            req_result = ""
            try:
                req_result = response_json['results'][0]['criteria']
            except IndexError:
                req_result = None
            result = {'name': test,
                      'result': req_result,
                      'url': response_url}
            LOGGER.debug("result: %s", result)
            nb_tests += 1
            if req_result == "PASS":
                nb_pass += 1
            LOGGER.debug("nb_pass: %s", nb_pass)
            LOGGER.debug("nb_tests: %s", nb_tests)
            score = round(100 * nb_pass / nb_tests)
            LOGGER.debug("score: %s", score)
            tier_results.append(result)

        tier['score'] = score
        tier['results'] = tier_results

    # calculation of the overall SCORE
    NB_TIERS = 0
    for tier in TIERS:
        NB_TIERS += 1
        LOGGER.debug("Score %s", tier)
        SCORE = SCORE + tier['score']
    SCORE = round(SCORE / NB_TIERS)
    LOGGER.info("Score: %s", str(SCORE))

    # calculation of the evolution score versus trend
    if SCORE > 1.05*SCORE_TREND:
        # increasing
        TREND_LINE = "long arrow alternate up icon"
        LOGGER.info("Better status")
    elif SCORE < 0.95*SCORE_TREND:
        # decreasing
        TREND_LINE = "long arrow alternate down icon"
        LOGGER.info("Worst status")
    else:
        # stable
        TREND_LINE = "long arrow alternate right icon"
        LOGGER.info("stable status")

    TEMPLATELOADER = jinja2.FileSystemLoader(".")
    TEMPLATEENV = jinja2.Environment(
        loader=TEMPLATELOADER, autoescape=True)
    TEMPLATE_FILE = ("./template/index-tmpl.html")
    TEMPLATE = TEMPLATEENV.get_template(TEMPLATE_FILE)
    OUTPUT_TEXT = TEMPLATE.render(
        tiers=TIERS,
        pod=pod,
        period=PERIOD,
        date=REPORTINGDATE,
        score=SCORE,
        trend=TREND_LINE)

    FILENAME = "./index.html"

    with open(FILENAME, "w+") as fh:
        fh.write(OUTPUT_TEXT)
