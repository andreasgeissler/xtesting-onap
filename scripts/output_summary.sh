#!/bin/bash

echo '_____________________________ Results ______________________'
echo ''
echo '************************************************************'
echo '************************************************************'
echo '************ Infrastructure-healthcheck Results ************'
echo '************************************************************'
echo '************************************************************'
echo '--------> onap-k8s'
grep '>>>' "./public/$1/infrastructure-healthcheck/k8s/onap-k8s.log" | tr ',' '\n' | sed 's/>>>/ */; s/\[\([^]]\)/[\'$'\n   - \\1/; s/^[^ ][^\*]*/   - &/'
echo '--------> onap-helm'
grep '>>>' "./public/$1/infrastructure-healthcheck/k8s/onap-helm.log" | tr ',' '\n' | sed 's/>>>/ */; s/\[\([^]]\)/[\'$'\n   - \\1/; s/^[^ ][^\*]*/   - &/'
echo ''
echo '************************************************************'
echo '************************************************************'
echo '********************* Healthcheck Results ******************'
echo '************************************************************'
echo '************************************************************'
if [ -f ./public/$1/healthcheck/core/xtesting.log ]; then
    echo '--------> robot  full healthcheck tests'
    sed -n '/xtesting.core.robotframework - INFO - $/,/Output/p' "./public/$1/healthcheck/full/xtesting.log"
    echo '--------> healthdist (vFW onboarding and distribution)'
    sed -n '/xtesting.core.robotframework - INFO - $/,/Output/p' "./public/$1/healthcheck/healthdist/xtesting.log"
    echo '--------> postinstall tests (dmaap and A&AI)'
    sed -n '/xtesting.core.robotframework - INFO - $/,/Output/p' "./public/$1/healthcheck/postinstall/xtesting.log"
    echo ''
else
    echo "Healthcheck tests not executed"
fi
echo ''

echo '************************************************************'
echo '************************************************************'
echo '********************** Smoke usecases **********************'
if [ -f ./public/$1/smoke-usecases/basic_vm/xtesting.log ]; then
echo '************************************************************'
echo '************************************************************'
    echo '--------> End to End tests (onap-tests)'
    for test in basic_vm freeradius_nbi clearwater_ims;do
        cat ./public/$1/smoke-usecases/$test/xtesting.log |tail |grep $test | grep -v ERROR | awk {'print $2 ": "  $8 " (" $6 ")"'}
    done
    cat ./public/$1/smoke-usecases-robot/pnf-registrate/xtesting.log |tail |grep pnf-registrate | grep -v -E -- 'DEBUG|INFO|ERROR' | awk {'print $2 ": "  $8 " (" $6 ")"'}
else
    echo "End to End VNF use cases not executed!"
fi
echo ''

echo '************************************************************'
echo '************************************************************'
echo '********************** Security tests **********************'
if [ -f ./public/$1/security/root_pods/xtesting.log ]; then
echo '************************************************************'
echo '************************************************************'
    for test in cis_kubernetes  http_public_endpoints  jdpw_ports  kube_hunter  root_pods  unlimitted_pods;do
        cat ./public/$1/security/$test/xtesting.log |tail |grep $test | grep -v -E -- 'DEBUG|INFO|ERROR' | awk {'print $2 ": "  $8 " (" $6 ")"'}
    done
    echo ''
    echo '--------> FOCUS on CIS errors'
    cat ./public/$1/security/cis_kubernetes/cis_kubernetes.log | grep FAIL
else
  echo 'Security tests not executed'
fi
echo ''
echo '____________________________________________________________'
